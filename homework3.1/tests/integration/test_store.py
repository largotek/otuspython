import os
import unittest
from unittest.mock import MagicMock

from store import RedisStorage

class TestStore(unittest.TestCase):
    def setUp(self):
        self.store = RedisStorage(host=os.getenv('REDIS_HOST', 'localhost'),
                                  port=int(os.getenv('REDIS_PORT', 6379)))
        self.key = 'key1'
        self.value = 'value1'
        self.store._connect()

    def test_store_connected(self):
        self.assertTrue(self.store._set(self.key, self.value))
        self.assertEqual(self.store._get(self.key), self.value)

    def test_store_disconnected(self):

        self.store.db.get = MagicMock(side_effect=ConnectionError())

        with self.assertRaises(ConnectionError):
            self.store.get(self.key)
        self.assertEqual(self.store.db.get.call_count, self.store.max_attempts)

    def test_cache_connected(self):
        self.assertTrue(self.store.cache_set(self.key, self.value))
        self.assertEqual(self.store.cache_get(self.key), self.value)

    def test_cache_disconnected(self):
        self.store.db.get = MagicMock(side_effect=ConnectionError())
        self.store.db.set = MagicMock(side_effect=ConnectionError())

        self.assertEqual(self.store.cache_get(self.key), None)
        self.assertEqual(self.store.cache_set(self.key, self.value), None)
        self.assertEqual(self.store.db.get.call_count, self.store.max_attempts)
        self.assertEqual(self.store.db.set.call_count, self.store.max_attempts)


if __name__ == "__main__":
    unittest.main()
