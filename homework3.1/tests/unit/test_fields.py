import hashlib
import datetime
import functools
import unittest

import api


def cases(cases):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args):
            for c in cases:
                try:
                    new_args = args + (c if isinstance(c, tuple) else (c,))
                    f(*new_args)
                except Exception as e:
                    print('This case raised exception -- ', c)
                    raise e
        return wrapper
    return decorator


def set_attr(obj, attr):
    obj.field = attr


class TestCharField(unittest.TestCase):
    field = api.CharField(required=False, nullable=False)

    @cases([2, 1.3])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases([ '2', 'test_case' ])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestEmailField(unittest.TestCase):
    field = api.EmailField(required=False, nullable=True)

    @cases([2, 1.3, 'test'])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases([ '@', 'test_case@', 'test@test'])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestClientIDsField(unittest.TestCase):
    field = api.ClientIDsField(required=False, nullable=True)

    @cases([[-2,1], [1.3,2], [3,'test'], [-2]])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases([[1,2], [1,2,6], [3], []])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestDateField(unittest.TestCase):
    field = api.DateField(required=False, nullable=True)

    @cases([1, -2, 'test', '14.14.1222', '1.2.222'])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases(['12.12.1222', '1.1.1999'])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestPhoneField(unittest.TestCase):
    field = api.PhoneField(required=False, nullable=True)

    @cases([7985294435, 89852944355, '+79852944355', '7985294435'])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases([79852944355, '79852944355'])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestBirthDayField(unittest.TestCase):
    field = api.BirthDayField(required=False, nullable=True)

    @cases([1, -2, 'test', '14.14.1222', '1.2.222', '1.2.1222'])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases(['12.12.1952', '1.1.1999'])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestGenderField(unittest.TestCase):
    field = api.GenderField(required=False, nullable=True)

    @cases([-1, -2.9, '1234','-5', 5, '5'])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases([1, 2, 0])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


class TestArgumentsField(unittest.TestCase):
    field = api.ArgumentsField(required=False, nullable=True)

    @cases(['{}', 1, [1,2]])
    def test_value_error(self, value):
        with self.assertRaises(ValueError):
            set_attr(self, value)

    @cases([{}, {1:2}])
    def test_ok(self, value):
        self.assertIsNone(set_attr(self, value))


if __name__ == "__main__":
    unittest.main()
