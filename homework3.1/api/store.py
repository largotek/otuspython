import functools
import time

import redis


class Store:

    def __init__(self, max_attempts=3, sleep_time=1):
        self.max_attempts = max_attempts
        self.sleep_time = sleep_time

    def get(self, key):
        return self.retry(self._get, False, key)

    def cache_get(self, key):
        return self.retry(self._get, True, key)

    def cache_set(self, key, value, expires=None):
        return self.retry(self._set, True, key, value, expires)

    def retry(self, f, silent, *args, **kwargs):
        for attempt in range(self.max_attempts):
            try:
                return f(*args, **kwargs)
            except (TimeoutError, ConnectionError):
                time.sleep(self.sleep_time)
        if not silent:
            raise ConnectionError

    def _get(self, key):
        raise NotImplementedError()

    def _set(self, key, value, expires=None):
        raise NotImplementedError()


class RedisStorage(Store):
    def __init__(self, host='localhost', port=6379, timeout=3, max_attempts=3, sleep_time=1):
        super().__init__(max_attempts, sleep_time)
        self.host = host
        self.port = port
        self.timeout = timeout
        self.db = None

    def _connect(self):
        self.db = redis.Redis(
            host=self.host,
            port=self.port,
            db=0,
            socket_connect_timeout=self.timeout,
            socket_timeout=self.timeout
        )

    def _get(self, key):
        if self.db is None:
            self._connect()
        try:
            value = self.db.get(key)
            return value.decode() if value else value
        except redis.exceptions.TimeoutError:
            raise TimeoutError
        except redis.RedisError:
            raise ConnectionError

    def _set(self, key, value, expires=None):
        if self.db is None:
            self._connect()
        try:
            return self.db.set(key, value, ex=expires)
        except redis.exceptions.TimeoutError:
            raise TimeoutError
        except redis.exceptions.ConnectionError:
            raise ConnectionError
