import hashlib
import json
import random
import logging

def get_score(store, phone, email, birthday=None, gender=None, first_name=None, last_name=None):

    key_parts = [
        first_name or "",
        last_name or "",
        str(phone) or "",
        birthday if birthday is not None else "",
    ]
    logging.info('----' , key_parts)

    print('---------------------------' , key_parts)
    key = 'uid:' + hashlib.md5(''.join(key_parts).encode()).hexdigest()
    # try get from cache,
    # fallback to heavy calculation in case of cache miss

    score = 0

    if store:
        score = store.cache_get(key) or 0

    if score:
        return score
    if phone:
        score += 1.5
    if email:
        score += 1.5
    if birthday and gender:
        score += 1.5
    if first_name and last_name:
        score += 0.5
    # cache for 60 minutes
    if store:
        store.cache_set(key, score, 60 * 60)
    return score


def get_interests(store, cid):
    r = store.get("i:%s" % cid)

    # interests = ["cars", "pets", "travel", "hi-tech", "sport", "music", "books", "tv", "cinema", "geek", "otus"]
    # r = json.dumps(random.sample(interests, 2))

    return json.loads(r) if r else []