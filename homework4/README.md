Web server test suite
=====================

Implement a simple asynchronous web server.

## Architecture ##

* for asynchronous was used asyncio
* it's possible to increase number of workers
* workers implemented as multiprocessing


## Result of stress test ##


### Empty request 

```
Finished 50000 requests


Server Software:        OTUS-server
Server Hostname:        127.0.0.1
Server Port:            8899

Document Path:          /
Document Length:        0 bytes

Concurrency Level:      100
Time taken for tests:   103.177 seconds
Complete requests:      50000
Failed requests:        0
Non-2xx responses:      50000
Total transferred:      7300000 bytes
HTML transferred:       0 bytes
Requests per second:    484.60 [#/sec] (mean)
Time per request:       206.354 [ms] (mean)
Time per request:       2.064 [ms] (mean, across all concurrent requests)
Transfer rate:          69.09 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0  150 1536.8      0   19696
Processing:     2   56  60.3     52    1113
Waiting:        1   48  54.4     44    1100
Total:          2  206 1536.3     53   19753
```

### Wikipedia
```
Server Software:        OTUS-server
Server Hostname:        127.0.0.1
Server Port:            8899

Document Path:          /httptest/wikipedia_russia.html
Document Length:        954824 bytes

Concurrency Level:      100
Time taken for tests:   696.670 seconds
Complete requests:      50000
Failed requests:        0
Total transferred:      47748350000 bytes
HTML transferred:       47741200000 bytes
Requests per second:    71.77 [#/sec] (mean)
Time per request:       1393.340 [ms] (mean)
Time per request:       13.933 [ms] (mean, across all concurrent requests)
Transfer rate:          66931.60 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0      15
Processing:    69 1393 232.6   1345    3177
Waiting:        5  194  67.8    185    1356
Total:         69 1393 232.6   1345    3177

Percentage of the requests served within a certain time (ms)
  50%   1345
  66%   1411
  75%   1461
  80%   1500
  90%   1637
  95%   1820
  98%   2136
  99%   2359
 100%   3177 (longest request)
```

### Tests
```
python httptest.py
directory index file exists ... ok
document root escaping forbidden ... ok
Send bad http headers ... ok
file located in nested folders ... ok
absent file returns 404 ... ok
urlencoded filename ... ok
file with two dots in name ... ok
query string after filename ... ok
filename with spaces ... ok
Content-Type for .css ... ok
Content-Type for .gif ... ok
Content-Type for .html ... ok
Content-Type for .jpeg ... ok
Content-Type for .jpg ... ok
Content-Type for .js ... ok
Content-Type for .png ... ok
Content-Type for .swf ... ok
head method support ... ok
directory index file absent ... ok
large file downloaded correctly ... ok
post method forbidden ... ok
Server header exists ... ok

----------------------------------------------------------------------
Ran 22 tests in 0.107s

OK
```