import asyncio
import argparse
from datetime import datetime
import logging
import mimetypes
import multiprocessing
import os
from urllib.parse import unquote, urlparse

HTTP_200_OK = 200
HTTP_400_BAD_REQUEST = 400
HTTP_403_FORBIDDEN = 403
HTTP_404_NOT_FOUND = 404
HTTP_405_METHOD_NOT_ALLOWED = 405

RESPONSE_CODES = {
    HTTP_200_OK: 'OK',
    HTTP_400_BAD_REQUEST: 'Bad Request',
    HTTP_403_FORBIDDEN: 'Forbidden',
    HTTP_404_NOT_FOUND: 'Not Found',
    HTTP_405_METHOD_NOT_ALLOWED: 'Method Not Allowed',
}

DOCUMENT_ROOT = './'
HEAD_TERMINATOR = '\r\n\r\n'
SERVER_NAME = 'OTUS-server'
PROTOCOL = 'HTTP/1.1'


class HTTPRequest(object):
    """
    HTTP request handler
    """
    methods = ('GET', 'HEAD')

    def __init__(self, document_root):
        self.document_root = document_root

    def parse(self, request_data):
        lines = request_data.split('\r\n')
        try:
            method, url, version = lines[0].split()
            method = method.upper()
        except ValueError:
            return HTTP_400_BAD_REQUEST, '?', '?', {}

        headers = {}
        for line in lines[1:]:
            if not line.split():
                break
            k, v = line.split(':', 1)
            headers[k.lower()] = v.strip()

        if method not in self.methods:
            return HTTP_405_METHOD_NOT_ALLOWED, method, url, headers

        code, path = self.parse_url(url)

        return code, method, path, headers

    def parse_url(self, url):
        parsed_path = unquote(urlparse(url).path)
        path = self.document_root + os.path.abspath(parsed_path)

        is_directory = os.path.isdir(path)
        if is_directory:
            if not path.endswith('/'):
                path += '/'
            path = os.path.join(path, 'index.html')

        if not is_directory and parsed_path.endswith('/'):
            return HTTP_404_NOT_FOUND, path
        if path.endswith('/') or not os.path.isfile(path):
            return HTTP_404_NOT_FOUND, path

        return HTTP_200_OK, path


class HTTPResponse(object):
    def __init__(self, code, method, path, request_headers):
        self.code = code
        self.method = method
        self.path = path
        self.request_headers = request_headers

    def process(self):

        # Prepare meta info
        file_size = 0
        content_type = 'text/plain'
        body = b''
        if self.code == HTTP_200_OK:
            file_size = self.request_headers.get('content-length', os.path.getsize(self.path))
            if self.method == 'GET':
                content_type = mimetypes.guess_type(self.path)[0]
                with open(self.path, 'rb') as file:
                    body = file.read(file_size)

        # Prepare response
        first_line = '{} {} {}'.format(PROTOCOL, self.code, RESPONSE_CODES[self.code])
        headers = {
            'Date': datetime.now().strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'Server': SERVER_NAME,
            'Connection': 'close',
            'Content-Length': file_size,
            'Content-Type': content_type,
        }
        headers = '\r\n'.join('{}: {}'.format(k, v) for k, v in headers.items())
        response = '{}\r\n{}{}'.format(first_line, headers, HEAD_TERMINATOR).encode() + body
        return response


class AsyncHTTPServer:
    request_handler = HTTPRequest(DOCUMENT_ROOT)

    def __init__(self, host, port, request_handler: HTTPRequest = None):
        self.host = host
        self.port = port
        self.request_handler = request_handler if request_handler else self.request_handler

    async def connect_handle(self, reader, writer):
        try:
            data = await reader.readuntil(HEAD_TERMINATOR.encode())
            message = data.decode()
            addr = writer.get_extra_info('peername')
            print("Received %r from %r" % (message, addr))

            request = HTTPRequest(DOCUMENT_ROOT)
            code, method, path, headers = request.parse(message)
            response = HTTPResponse(code, method, path, headers)
            response_data = response.process()

            print("Send: %r" % message)
            writer.write(response_data)
            await writer.drain()
        except Exception as ex:
            print(f'   ------- [ {ex}] Error')
        finally:
            print("Close the client socket")
            writer.close()

    def serve_forever(self):

        loop = asyncio.get_event_loop()
        coro = asyncio.start_server(self.connect_handle, self.host, self.port, loop=loop,
                                    reuse_address=None, reuse_port=True, backlog=200)
        server = loop.run_until_complete(coro)

        # Serve requests until Ctrl+C is pressed
        print('Serving on {}'.format(server.sockets[0].getsockname()))
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass

        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()


def parse_args():
    parser = argparse.ArgumentParser("Simple asynchronous web-server")
    parser.add_argument("--host", dest="host", default="0.0.0.0")
    parser.add_argument("--port", dest="port", type=int, default=9000)
    parser.add_argument("--log", dest="loglevel", default="info")
    parser.add_argument("--logfile", dest="logfile", default=None)
    parser.add_argument("-w", dest="nworkers", type=int, default=3)
    parser.add_argument("-r", dest="document_root", default=".")
    return parser.parse_args()


def run():
    server = AsyncHTTPServer(host=args.host, port=args.port)
    server.serve_forever()


if __name__ == "__main__":
    args = parse_args()

    logging.basicConfig(
        filename=args.logfile,
        level=getattr(logging, args.loglevel.upper()),
        format="%(name)s: %(process)d %(message)s")
    log = logging.getLogger(__name__)

    DOCUMENT_ROOT = args.document_root
    for _ in range(args.nworkers):
        p = multiprocessing.Process(target=run)
        p.start()
