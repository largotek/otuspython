#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import json
# import datetime
import logging
import hashlib
import uuid
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler

from abc import ABC, abstractmethod
from datetime import datetime

# from homework3.scoring import get_score, get_interests
from scoring import get_score, get_interests

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class ValidatedField(ABC):

    empty_values = (None, '', [], (), {})

    @abstractmethod
    def validate(self, value):
        raise NotImplementedError

    def __init__(self,  required, nullable):
        # self.validate = validate
        self.required = required
        self.nullable = nullable

    def __get__(self, instance, owner):
        return instance.__dict__.get(self.name, None)

    def __set__(self, instance, value):
        logging.info(f"Set attribute server name - {self.name}, value - {value}")

        if (value is None and self.required):
            raise ValueError('Required field')
        if (value in self.empty_values) and (not self.nullable):
            raise ValueError("The field can't be empty.")
        
        if (value is not None):
            self.validate(value)
            instance.__dict__[self.name] = value

    def __set_name__(self, owner, name):
        self.name = name


class CharField(ValidatedField):

    def validate(self, value):
        if type(value) != str:
            raise ValueError('The field must be a string type')
        

class ArgumentsField(ValidatedField):

    def validate(self, value):
        if type(value) != dict:
            raise ValueError('The field must be a dict type')


class EmailField(ValidatedField):
    type_description = 'The field should be of str type and have @'

    def validate(self, value):
        if (type(value) != str):
            raise ValueError('The field must be a string type')
        if ('@' not in value):
            raise ValueError('The field should include the symbol @')


class PhoneField(ValidatedField):
    type_description = 'string or number, length 11, starts with 7, optionally, can be empty'

    def validate(self, value):
        if not isinstance(value, (str, int)):
            raise TypeError("This field must be a number or a string")

        if isinstance(value, (str)) and (not value.isdigit()):
            ValueError("This field should only contain numbers.")

        if not str(value).startswith("7"):
            raise ValueError("Wrong phone number, number should start with 7")

        if len(str(value)) != 11:
            raise ValueError("Wrong phone number, the number must contain 11 digits")


class DateField(ValidatedField):
    format = "%d.%m.%Y"

    def strptime(self, value, format):
        return datetime.strptime(value, format)

    def validate(self, value):
        if not isinstance(value, (str)):
            raise TypeError("This field must be set to the line")
        try:
            return self.strptime(value, self.format).date()
        except ValueError:
            raise ValueError("The date must be in DD.MM.YYYY format.")


class BirthDayField(DateField):
    type_description = 'the DD.MM.YYYY date, with which no more than 70 years have passed, may optionally be blank'

    def validate(self, value):
        super().validate(value)

        age = (datetime.now() - self.strptime(value, self.format)).days / 365
        if not (0 < age <= 70):
            raise ValueError('The age should be within 0 < value <= 70')


class GenderField(ValidatedField):
    type_description = 'number 0, 1 or 2, optionally, can be blank'

    def validate(self, value):
        if type(value) is not int:
            raise ValueError("This field must be given by the number")
        if int(value) not in [0, 1, 2]:
            raise ValueError("This field must be set to 0, 1 or 2.")


class ClientIDsField(ValidatedField):

    def validate(self, value):
        if not isinstance(value, list) or not all(isinstance(v, int) for v in value):
                raise TypeError("This field should contain a list of integers")

        if not all(v >=0 for v in value):
            raise ValueError("This field should consist of positive integers.")


class BaseRequest(ABC):
    def __init__(self, request):
        self._errors = {}
        self.non_empty_fields = []

        for attribute in self.__class__.__dict__:
            
            if not isinstance(self.__class__.__dict__[attribute], ValidatedField):
                continue
            value = request.get(attribute, None)

            try:
                setattr(self, attribute, value)
                if value is not None:
                    self.non_empty_fields.append(attribute)
                    logging.info(f'Attribute was setted {attribute}')

            except (TypeError, ValueError) as e:
                self._errors[attribute] = str(e)
    
    def get_response(self):
        if self._errors:
            return self._errors, INVALID_REQUEST
    
    @property
    def errors(self):
        return self._errors


class ClientsInterestsRequest(BaseRequest):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)

    def __init__(self, request, context, store):
        super().__init__(request.arguments)

        if self._errors:
            return
        
        context["nclients"] = len(self.client_ids)
        self.response = {i: get_interests(store, i) for i in self.client_ids}

    def get_response(self):
        response = super().get_response()
        if response: return response

        return self.response, OK


class OnlineScoreRequest(BaseRequest):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)
    
    def check_pairs(self):
        if self.phone and self.email: return
        if self.first_name and self.last_name: return
        if self.gender is not None and self.birthday: return
        self._errors["arguments"] = "Wrong list of arguments"

    def __init__(self, request, context, store):
        super().__init__(request.arguments)

        if self._errors:
            return

        self.check_pairs()

        if request.is_admin:
            self.score = 42
        else:
            self.score = get_score(store, self.phone, 
                                    self.email, self.birthday, 
                                    self.gender, self.first_name, 
                                    self.last_name)
        context["has"] = self.non_empty_fields

    def get_response(self):
        response = super().get_response()
        if response: return response

        return {"score": self.score}, OK


class MethodRequest(BaseRequest):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN

def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(bytes(datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT, "utf-8")).hexdigest()
    else:
        digest = hashlib.sha512(bytes(request.account + request.login + SALT, "utf-8")).hexdigest()
    if digest == request.token:
        return True
    return False

def method_handler(request, ctx, store):
    response, code = None, None

    if request.get('body', None) is None:
        return 'Body was not found', INVALID_REQUEST

    methods = {
        'online_score' : OnlineScoreRequest,
        'clients_interests': ClientsInterestsRequest,
    }

    method_request = MethodRequest(request['body'])
    if method_request._errors:
        return method_request.errors, INVALID_REQUEST 
    if not check_auth(method_request):
        return "Forbidden", FORBIDDEN

    if methods.get(method_request.method, None) is None:
        return 'Method request was not found', INVALID_REQUEST

    handler = methods[method_request.method](method_request, ctx, store)
    return handler.get_response()


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(bytes(json.dumps(r), "utf-8"))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
