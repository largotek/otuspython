# README #

запуск в директории homework3

```
python api.py
```


Вызов help
```
$ python3 api.py -h

Usage: api.py [options]

Scoring API

Options:
  -h, --help            show this help message and exit
  -p PORT, --port=PORT  Port binding
  -l LOG, --log=LOG     Log file path

```

запуск тестов так же из директории homework3

```
python test.py
```


## Структура ответа

Ответы на валидный запрос
```
{
    "code": <response numeric code>,
    "response": {
        <method response data>
    },
}
```

Если не валидный

```
{
    "code": <response numeric code>, 
    "error": {
        <error message>
    }
}
```

Пример запроса

```
curl -X POST -H "Content-Type: application/json" -d '{
	"account": "horns&hoofs",
	"login": "h&f",
	"method": "online_score",
	"token": "55cc9ce545bcd144300fe9efc28e65d415b923ebb6be1e19d2750a2c03e80dd209a27954dca045e5bb12418e7d89b6d718a9e35af34e14e1d5bcd5a08f21fc95",
	"arguments": {
		"phone": "79175002040",
		"email": "stupnikov@otus.ru",
		"first_name": "Стансилав",
		"last_name": "Ступников",
		"birthday": "01.01.1990",
		"gender": 1
	}
}' http://127.0.0.1:8080/method/
# {"response": {"score": 5.0}, "code": 200} 
```
