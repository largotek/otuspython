#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------
# Реализуйте функцию best_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. У каждой карты есть масть(suit) и
# ранг(rank)
# Масти: трефы(clubs, C), пики(spades, S), червы(hearts, H), бубны(diamonds, D)
# Ранги: 2, 3, 4, 5, 6, 7, 8, 9, 10 (ten, T), валет (jack, J), дама (queen, Q), король (king, K), туз (ace, A)
# Например: AS - туз пик (ace of spades), TH - дестяка черв (ten of hearts), 3C - тройка треф (three of clubs)

# Задание со *
# Реализуйте функцию best_wild_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. Кроме прочего в данном варианте "рука"
# может включать джокера. Джокеры могут заменить карту любой
# масти и ранга того же цвета, в колоде два джокерва.
# Черный джокер '?B' может быть использован в качестве треф
# или пик любого ранга, красный джокер '?R' - в качестве черв и бубен
# любого ранга.

# Одна функция уже реализована, сигнатуры и описания других даны.
# Вам наверняка пригодится itertools.
# Можно свободно определять свои функции и т.п.
# -----------------

from itertools import combinations

ranks = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14}


def hand_rank(hand):
    """Возвращает значение определяющее ранг 'руки'"""
    ranks = card_ranks(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks), kind(1, ranks))
    elif kind(3, ranks) and kind(2, ranks):
        return (6, kind(3, ranks), kind(2, ranks))
    elif flush(hand):
        return (5, ranks)
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, ranks):
        return (3, kind(3, ranks), ranks)
    elif two_pair(ranks):
        return (2, two_pair(ranks), ranks)
    elif kind(2, ranks):
        return (1, kind(2, ranks), ranks)
    else:
        return (0, ranks)


def card_ranks(hand):
    """Возвращает список рангов (его числовой эквивалент),
    отсортированный от большего к меньшему"""
    return sorted([ranks[x[0]] for x in hand])


def flush(hand):
    """Возвращает True, если все карты одной масти"""
    return len(set([x[1] for x in hand])) == 1


def straight(ranks):
    """Возвращает True, если отсортированные ранги формируют последовательность 5ти,
    где у 5ти карт ранги идут по порядку (стрит)"""

    count_str = 0
    for i in range(len(ranks) -1):
        if (ranks[i+1] - ranks[i]) == 1:
            count_str += 1
        else:
            count_str = 0

    return count_str == 4


def kind(n, ranks):
    """Возвращает первый ранг, который n раз встречается в данной руке.
    Возвращает None, если ничего не найдено"""
    for rank in ranks:
        if ranks.count(rank) == n:
            return rank

    return None


def two_pair(ranks):
    """Если есть две пары, то возврщает два соответствующих ранга,
    иначе возвращает None"""

    set_ranks = []
    for rank in ranks:
        if ranks.count(rank) == 2:
            if rank not in set_ranks:
                set_ranks.append(rank)

    return set_ranks if len(set_ranks) == 2 else None


def card_to_rank(hand):
    return [x.replace(x[0], str(ranks[x[0]])) for x in hand]


def rank_to_card(hand):
    inv_ranks = {str(v): k for k, v in ranks.items()}
    return [x.replace(x[:-1], inv_ranks[x[:-1]]) for x in hand]


def get_best_hands(dict_combinations):
    best_hands = sorted(dict_combinations.items(), key=lambda k: k[1], reverse=True)
    max_rank = best_hands[0][1]
    best_hands = [x for x in best_hands if max_rank == x[1]]
    best_hands = [x[0] for x in best_hands]
    best_hands = sorted(best_hands, key= lambda x: [int(i[:-1]) for i in x ], reverse=True)
    return best_hands, max_rank


def best_hand_rank(hand):
    dict_comb = {}
    for comb in combinations(hand, 5):
        key_comb = tuple(sorted(card_to_rank(comb)))
        dict_comb[key_comb] = hand_rank(comb)[0]

    best_hands, max_rank = get_best_hands(dict_comb)

    return best_hands[0], max_rank


def best_hand(hand):
    """Из "руки" в 7 карт возвращает лучшую "руку" в 5 карт """

    res, score = best_hand_rank(hand)
    res = rank_to_card(res)

    return res


def replace_joker(hand):
    new_hands = []

    if '?B' in hand:
        for x in ['S', 'C']:
            for rank in list(ranks.keys()):
                new_hands.append( [f'{rank}{x}' if i == '?B' else i for i in hand])

    if '?R' in hand:
        for x in ['H', 'D']:
            for rank in list(ranks.keys()):
                new_hands.append( [f'{rank}{x}' if i == '?R' else i for i in hand])

    final_hands = []

    for one_hand in new_hands:
        if '?B' in one_hand:
            for x in ['S', 'C']:
                for rank in list(ranks.keys()):
                    final_hands.append([f'{rank}{x}' if i == '?B' else i for i in one_hand])

        if '?R' in one_hand:
            for x in ['H', 'D']:
                for rank in list(ranks.keys()):
                    final_hands.append([f'{rank}{x}' if i == '?R' else i for i in one_hand])
        if ('?R' not in one_hand) and ('?B' not in one_hand):
            final_hands.append(one_hand)

    return final_hands


def best_wild_hand(hand):
    """best_hand но с джокерами"""
    if sum([1 for x in hand if '?' in x ]) > 0:
        hands = replace_joker(hand)
    else:
        return best_hand(hand)

    dict_hands_score = {}
    for x in hands:
        res, score = best_hand_rank(x)
        dict_hands_score[tuple(res)] = score

    best_hands, max_rank = get_best_hands(dict_hands_score)
    res = rank_to_card(best_hands[0])
    return list(res)


def test_best_hand():
    print( "test_best_hand...")
    assert (sorted(best_hand("6C 7C 8C 9C TC 5C JS".split()))
            == ['6C', '7C', '8C', '9C', 'TC'])
    assert (sorted(best_hand("TD TC TH 7C 7D 8C 8S".split()))
            == ['8C', '8S', 'TC', 'TD', 'TH'])
    assert (sorted(best_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    assert (sorted(best_hand("6C 7C 8C 9C TC 5C JC".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])

    print('OK')


def test_best_wild_hand():
    print("test_best_wild_hand...")
    assert (sorted(best_wild_hand("6C 7C 8C 9C TC 5C ?B".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])
    assert (sorted(best_wild_hand("TD TC 5H 5C 7C ?R ?B".split()))
            == ['7C', 'TC', 'TD', 'TH', 'TS'])
    assert (sorted(best_wild_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


if __name__ == '__main__':
    test_best_hand()
    test_best_wild_hand()
